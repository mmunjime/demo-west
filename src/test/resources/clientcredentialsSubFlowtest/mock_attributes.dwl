{
  "headers": {
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "cache-control": "no-cache",
    "postman-token": "de9095ba-4fda-42b6-8928-88ecd095af32",
    "host": "localhost:8092",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "0"
  },
  "clientCertificate": null,
  "method": "POST",
  "scheme": "https",
  "queryParams": {},
  "requestUri": "/test",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/test",
  "relativePath": "/test",
  "localAddress": "/127.0.0.1:8092",
  "uriParams": {},
  "rawRequestUri": "/test",
  "rawRequestPath": "/test",
  "remoteAddress": "/127.0.0.1:56313",
  "requestPath": "/test"
}