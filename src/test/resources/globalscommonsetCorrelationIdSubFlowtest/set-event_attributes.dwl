{
  "headers": {
    "user-agent": "PostmanRuntime/7.28.0",
    "accept": "*/*",
    "cache-control": "no-cache",
    "postman-token": "6b131a16-c397-4761-b385-7ac19cd45c7a",
    "host": "localhost:8092",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {},
  "requestUri": "/",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/",
  "relativePath": "/",
  "localAddress": "/127.0.0.1:8092",
  "uriParams": {},
  "rawRequestUri": "/",
  "rawRequestPath": "/",
  "remoteAddress": "/127.0.0.1:65525",
  "requestPath": "/"
}